﻿Type=Class
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
#Event: Append (Folder As String) As ChildTheSequence
#Event: ToString As String

Sub Class_Globals
	Private fx As JFX
	Private TheSequence As StringBuilder
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	TheSequence.Initialize
End Sub

Public Sub Append (Folder As String) As ChildSequence
	TheSequence.Append(Folder).Append(SpecialFolder.Separator)	
	Return Me
End Sub

Public Sub ToString As String
	TheSequence.Remove(TheSequence.Length - 1, TheSequence.Length)
	
	If Computer.IsWindows = True And TheSequence.ToString.Contains("C:\") = False Then
		TheSequence.Insert(0, "C:\")
	Else If Computer.IsLinux Or Computer.IsMac Then
		TheSequence.Insert(0, "/")
	End If
	
	Dim Output As String = TheSequence.ToString
	TheSequence.Remove(0, TheSequence.Length)

	If Directory.DirExists(Output) Then
		Return Output
	Else
		Return Null
	End If
End Sub