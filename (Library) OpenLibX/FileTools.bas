﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private fx As JFX
End Sub

'Counts the number of lines of a file.
Public Sub CountLines (Source As String) As Int
	Dim Lines() As String = Regex.Split(CRLF, Source)
	Dim Count As Int = 0
	For Each A As String In Lines
		LogDebug(A)
		Count = Count + 1
	Next
	Return Count
End Sub

'Splits and then counts the number of lines of a file.
Public Sub SplitAndCountLines (Split As String, Source As String) As Int
	Dim Index As Int = Source.LastIndexOf(Split)
	Dim Lines As String = Source.SubString(Index)
	Return CountLines(Lines)
End Sub