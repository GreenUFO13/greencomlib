﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private fx As JFX
	Private Info As MF_Info
End Sub

Public Sub IsWindows As Boolean
	If Name.ToLowerCase.Contains("win") Then
		Return True
	Else
		Return False
	End If
End Sub

Public Sub IsLinux As Boolean
	If Name.ToLowerCase.Contains("lin") Then
		Return True
	Else
		Return False
	End If
End Sub

Public Sub IsMac As Boolean
	If Name.ToLowerCase.Contains("mac") Then
		Return True
	Else
		Return False
	End If
End Sub

Public Sub DeviceActiveUser As String
	Return GetSystemProperty("user.name", "")
End Sub

Public Sub Version As Int
	Return Info.SystemVersion
End Sub

Public Sub Name As String
	Return Info.SystemName
End Sub

Public Sub DeviceArchitecture As String
	Return Info.SystemArchitecture
End Sub

Public Sub DeviceRegion As String
	Return Info.Region
End Sub