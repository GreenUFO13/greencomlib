﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private fx As JFX
End Sub

'Fades a node based on provided options.
Public Sub FadeNode(Node As Node, Length As Double, FromValue As Int, ToValue As Int)
	Dim Fade As FadeTransition
	Fade.Initialize(Main, Length, Node, "")
	Fade.FromValue = FromValue
	Fade.ToValue = ToValue
	Fade.Play
End Sub