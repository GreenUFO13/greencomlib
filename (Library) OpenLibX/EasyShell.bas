﻿Type=Class
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
Sub Class_Globals
	Private fx As JFX
	Private ProcessShell As Shell
	Private TheResult As StringBuilder
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	
End Sub

'Executes a command.
'Executable: The executable to use. (If you're using Command Prompt please provide any capitalization of "cmd".)
'Command: The command to execute.
Public Sub Execute (Executable As String, Command As String)
	TheResult.Initialize
	TheResult.Remove(0, TheResult.Length)
	Dim Args As List
	Args.Initialize
		
	If Executable.ToLowerCase.Contains("cmd") Then
		Executable = "C:\Windows\System32\cmd.exe"
		Args.Add("/c")
	End If
	
	Dim ResCommand() As String = Regex.Split($"[ ]+(?=([^"]*"[^"]*")*[^"]*$)"$, Command)
	For Each T As String In ResCommand
		Args.Add(T)
	Next
	ProcessShell.Initialize("", Executable, Args)
	Dim Result As ShellSyncResult = ProcessShell.RunSynchronous(-1)
	If Result.Success Then
		TheResult.Append(Result.StdErr).Append(Result.StdOut)
	Else
		TheResult.Append(Null)
	End If
End Sub

'Returns the output from the executed command. Returns null if the command failed.
Public Sub ReadAll As String
	Return TheResult
End Sub