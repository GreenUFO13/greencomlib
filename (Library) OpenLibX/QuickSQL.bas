﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
End Sub

Public Sub Initialize (Dir As String, FileName As String, CreateIfNeccessary As Boolean) As SQL
	Dim TheSQL As SQL
	#If Not (B4J)
	TheSQL.Initialize(Dir, FileName, CreateIfNeccessary)
	#Else
	TheSQL.InitializeSQLite(Dir, FileName, CreateIfNeccessary)
	#End If
	Return TheSQL
End Sub