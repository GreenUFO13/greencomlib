﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
#IgnoreWarnings: 17
Sub Process_Globals
	Private fx As JFX
End Sub

'Aligns the ListView items to the left.
Public Sub ListViewAlignLeft (Form As Form, ListView As ListView)
	ListView.Id = "left"
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "jufolib.css"))
End Sub

'Aligns the ListView items to the center.
Public Sub ListViewAlignCenter (Form As Form, ListView As ListView)
	ListView.Id = "center"
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "jufolib.css"))
End Sub

'Aligns the ListView items to the right.
Public Sub ListViewAlignRight (Form As Form, ListView As ListView)
	ListView.Id = "right"
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "jufolib.css"))
End Sub

'Aligns the ListView items vertically.
Public Sub ListViewAlignVertical (ListView As ListView)
	Dim Jo As JavaObject = ListView
	Jo.RunMethod("setOrientation", Array ("VERTICAL"))
End Sub

'Aligns the ListView items horizontally.
Public Sub ListViewAlignHorizontal (LV As ListView)
	Dim Jo As JavaObject = LV
	Jo.RunMethod("setOrientation", Array("HORIZONTAL"))
End Sub

'Aligns the ComboBox items to the center.
Public Sub ComboBoxAlignLeft (Form As Form, ComboBox As ComboBox)
	ComboBox.Id = "left"
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "jufolib.css"))
End Sub

'Aligns the ComboBox items to the center.
Public Sub ComboBoxAlignCenter (Form As Form, ComboBox As ComboBox)
	ComboBox.Id = "center"
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "jufolib.css"))
End Sub

'Aligns the ComboBox items to the center.
Public Sub ComboBoxAlignRight (Form As Form, ComboBox As ComboBox)
	ComboBox.Id = "right"
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "jufolib.css"))
End Sub

'Applies the caspian theme.
Public Sub ApplyCaspian(Form As Form)
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "caspian.css"))
End Sub

'Applies a dark version of the default "Modena" theme.
'Credit: https://github.com/joffrey-bion/javafx-themes/blob/master/css/modena_dark.css
Public Sub ApplyModenaDark (Form As Form)
	Form.Stylesheets.Add(File.GetUri(File.DirAssets, "modena-dark.css"))
End Sub

'Reloads the form.
Public Sub Reload (Form As Form)
	Form.Close
	Form.Show
End Sub

'Centers the form on screen.
Public Sub CenterForm (Form As Form)
   Dim ps As Screen = fx.PrimaryScreen
   Form.WindowTop = (ps.MaxY - ps.MinY) / 2 - Form.Height / 2
   Form.WindowLeft = (ps.MaxX - ps.MinX) / 2 - Form.Width / 2
End Sub

'Programmatically maximizes the form.
Public Sub MaximizeForm (Form As Form)
	Dim mf As JavaObject = Form
	mf.GetFieldJO("stage").RunMethod("setMaximized", Array(True))
End Sub

'Programmatically minimizes the form.
Public Sub MinimizeForm (Form As Form)
	Dim mf As JavaObject = Form
	mf.GetFieldJO("stage").RunMethod("setMinimized", Array(True))
End Sub

Public Sub SeparatorMenuItem As MenuItem
	Dim Jo As JavaObject = Me
	Return Jo.RunMethod("addSMI", Null)
	#If Java
	public static Separator addSeparator {
		Separator sep1 = new Separator();
		sep1.setOrientation(Orientation.HORIZONTAL);
		return sep1;
	}
	#End If
End Sub

'Disables a Menu Item by tag
Public Sub DisableMenuItemByTag (TheMenu As Menu, Tag As String)
	For Each Mi As MenuItem In TheMenu.MenuItems
		If Mi.Tag = Tag Then
			Mi.Enabled = False
		End If
	Next
End Sub

'Enables a Menu Item by tag
Public Sub EnableMenuItemByTag (TheMenu As Menu, Tag As String)
	For Each Mi As MenuItem In TheMenu.MenuItems
		If Mi.Tag = Tag Then
			Mi.Enabled = True
		End If
	Next
End Sub

'Disables a Menu Item by its text.
Public Sub DisableMenuItemByText (TheMenu As Menu, Text As String)
	For Each Mi As MenuItem In TheMenu.MenuItems
		If Mi.Text.Contains(Text) Then
			Mi.Enabled = False
		End If
	Next
End Sub

'Enables a Menu Item by its text.
Public Sub EnableMenuItemByText (TheMenu As MenuBar, Text As String)
	For Each Mi As MenuItem In TheMenu.Menus
		If Mi.Text.Contains(Text) Then
			Mi.Enabled = True
		End If
	Next
End Sub