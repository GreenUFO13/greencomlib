﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private CS As ChildSequence
	Private fx As JFX
End Sub

Private Sub Initialize
	CS.Initialize
End Sub

Public Sub Separator As String
	Return GetSystemProperty("file.separator","")
End Sub

Public Sub Documents As String
	Initialize
	If Computer.IsWindows And Computer.Version < 8 Then
		Return CS.Append(UserHome).Append("My Documents").ToString
	Else
		Return CS.Append(UserHome).Append("Documents").ToString
	End If
End Sub

Public Sub Downloads As String
	Initialize
	If Computer.IsWindows And Computer.Version < 8 Then
		Return CS.Append(UserHome).Append("My Downloads").ToString
	Else
		Return CS.Append(UserHome).Append("Downloads").ToString
	End If
End Sub

Public Sub Videos As String
	Initialize
	If Computer.IsWindows And Computer.Version < 8 Then
		Return CS.Append(UserHome).Append("My Videos").ToString
	Else If Computer.IsMac Then
		Return CS.Append(UserHome).Append("Movies").ToString
	Else
		Return CS.Append(UserHome).Append("Downloads").ToString
	End If
End Sub

Public Sub Pictures As String
	Initialize
	If Computer.IsWindows And Computer.Version < 8 Then
		Return CS.Append(UserHome).Append("My Pictures").ToString
	Else
		Return CS.Append(UserHome).Append("Pictures").ToString
	End If
End Sub

Public Sub UserHome As String
	Return GetSystemProperty("user.home","")
End Sub

'Returns the root directory of Steam.
Public Sub SteamRoot As String
	If Computer.IsWindows Then
		Return "C:\Program Files (x86)\Steam"
	Else If Computer.IsMac Then
		Return UserHome & "/Library/Application Support/Steam/Steam.AppBundle/Steam/Contents/MacOS/skins/"
	Else If Computer.IsLinux Then
		Return "/.local/share/Steam"
	Else
		Return Null
	End If
End Sub

'Returns the "Computermon" directory where all your Steam games live.
Public Sub SteamCommonFolder As String
	Return File.Combine(SteamRoot, "Common")
End Sub

'Returns the path to one of your Steam games.
Public Sub SteamGameFolder (App As String) As String
	Return File.Combine(SteamCommonFolder, App)
End Sub

Public Sub SystemRoot As String
	Initialize
	Dim Root As String
	If Computer.IsWindows Then
		Root = "C:\"
	Else
		Root = "/"
	End If
	Return Root	
End Sub

Public Sub WindowsDir As String
	If Computer.IsWindows = True Then
		Return GetEnvironmentVariable("windir","")
	Else
		Return Null
	End If
End Sub

Public Sub ProgramFiles32Bit As String
	If Computer.IsWindows Then
		Return $"${ProgramFiles64Bit} (x86)"$
	Else
		Return Null
	End If
End Sub

Public Sub ProgramFiles64Bit As String
	If Computer.IsWindows Then 
		Return GetEnvironmentVariable("ProgramFiles","")
	Else
		Return Null
	End If
End Sub

Public Sub StartupFolder As String
	Initialize
	If Computer.IsWindows Then
		Return CS.Append(UserHome).Append("AppData").Append("Roaming").Append("Microsoft").Append("Windows").Append("Start Menu").Append("Programs").Append("Startup").ToString
	Else
		Return Null
	End If
End Sub