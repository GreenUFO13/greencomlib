﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
Sub Process_Globals
	Private fx As JFX
	Private Info As MF_File
	Private FTools As fFILETOOLS
End Sub

'Tests to see whether a Dir exists.
Public Sub DirExists(DirPath As String) As Boolean
	Dim Parent, Child As String
	Parent = File.GetFileParent(DirPath)
	Child = File.GetName(DirPath)
	Return File.Exists(Parent, Child)
End Sub

'Makes a Dir at the root folder.
Public Sub MakeRootDir (DirPath As String)
	File.MakeDir(SpecialFolder.SystemRoot, DirPath)
End Sub

'Deletes a directory.
Public Sub DeleteDir (Dir As String) As Boolean
	DeleteDirRecursive (Dir)
	File.Delete(File.GetFileParent(Dir), File.GetName(Dir))
	If File.Exists(File.GetFileParent(Dir), File.GetName(Dir)) = False Then
		Return True
	Else
		Return Null
	End If
End Sub

'Deletes the contents of a directory
'Credit: Erel.
Private Sub DeleteDirRecursive(Dir As String)
   For Each f As String In File.ListFiles(Dir)
     If File.IsDirectory(Dir, f) Then
       DeleteDirRecursive (File.Combine(Dir, f))
     End If
     File.Delete(Dir, f)
   Next
End Sub

'Moves a Dir.
Public Sub MoveDir (OldDir As String, NewDir As String, ReplaceExisting As Boolean)
	Info.Move(OldDir, "", NewDir, "", ReplaceExisting)
End Sub

'Copies a Dir. 
Public Sub CopyDir(OldDir As String, NewDir As String)
	FTools.CopyDirectory(OldDir, NewDir)
End Sub

'Renames a Dir.
Public Sub RenameDir (OldDir As String, NewDir As String)
	CopyDir(OldDir, NewDir)
	DeleteDir(OldDir)
End Sub

'Checks to see if a Dir is a Dir.
Public Sub IsDir (DirPath As String) As Boolean
	Dim Parent, Child As String
	Parent = File.GetFileParent(DirPath)
	Child = File.GetName(DirPath)
	Return File.Exists(Parent, Child)
End Sub